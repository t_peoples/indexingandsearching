import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Lexicon {

    TreeMap<String, ArrayList<Long>> lexiconMap = new TreeMap<String,ArrayList<Long>>();
    long fileLength;
    long invertedListRef;
    long documentFrequency;
    String contentTerm = null;
    File file;

    public void lexicon(String fileName){
        file =new File(fileName);
    }
    public void setFile(String fileName){
        file =new File(fileName);
    }
    //Each item in map contains a ordinal number and a document identifier
    //Read lexicon from file
    public void readLexicon() throws IOException
    {

        //if file doesnt exists, then create it
        if(!file.exists())
        {
            file.createNewFile();
        }

        RandomAccessFile lexiconFile = new RandomAccessFile( file, "r" );
        fileLength = lexiconFile.length();

        lexiconFile.seek(0);
        try
        {
            while (true)
            {
                String contentTerm = lexiconFile.readUTF();
                long dPointer = lexiconFile.readLong();
                long documentFrequency = lexiconFile.readLong();

                ArrayList<Long> lexiconVariables = new ArrayList<Long>();
                lexiconVariables.add(dPointer);
                lexiconVariables.add(documentFrequency);

                lexiconMap.put(contentTerm,lexiconVariables);
            }
        }
        catch (EOFException ex)
        {
            System.out.println("End of file reached!");
        }
        finally
        {
            // Close the file to release file resources.
            lexiconFile.close();
        }
    }
    //Get filepointer from lexicon
    public long getPointer(String contentTerm)
    {
        ArrayList<Long> list = lexiconMap.get(contentTerm);
        long pointer = list.get(0);
        return pointer;
    }
    //Get document frequency from lexicon
    public long getDocumentFrequency(String contentTerm)
    {
        ArrayList<Long> list = lexiconMap.get(contentTerm);
        long documentFrequency = list.get(1);
        return documentFrequency;
    }
    //Check if content already exits in lexicon
    public boolean checkForContentTerm(String contentTerm)
    {
        return lexiconMap.containsKey(contentTerm);
    }
    //TODO Refactor this function might warrant its own class
    //Method used for printing the lexicon
    public void printLexicon()
    {
        for(Entry<String, ArrayList<Long>> entry : lexiconMap.entrySet())
        {
            System.out.print("ContentTerm: " + entry.getKey()+",FilePointer & Document Frequency ");
            for(long value : entry.getValue())
            {
                System.out.print(value + " ");
            }
            System.out.println(" ");
        }
    }


}
