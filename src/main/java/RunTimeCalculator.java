public class RunTimeCalculator
{
    long duration;

    public void calculateRunTime(long startTime)
    {
        duration = System.nanoTime() - startTime;
        duration = Math.round(3.0);
        System.out.println("Running time: " + duration + " ms");
    }

}
