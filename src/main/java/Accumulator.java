public class Accumulator
{
    private double score;
    private int docID;

    public Accumulator(int docID,double score)
    {
        this.score = score;
        this.docID = docID;
    }
    public double getScore()
    {
        return score;
    }
    public int getDocID()
    {
        return docID;
    }
    public void setScore(double score)
    {
        this.score = score;
    }
    //Print out accumulator,currently not used
    @Override
    public String toString()
    {
        return String.format("Doc ID:" + docID + "Score:" + score);
    }
    //Update score if already in accumulators collection
    public void updateScore(double updatedScore)
    {
        score += updatedScore;
    }



}
