import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;

public class ProcessQuery {

    String queryTerm = null;
    String invFile = null;
    int documentFrequency;
    int documentLength;
    int documentID;
    long filePointer;
    double N;
    int interDocumentFrequency;
    double similarity;
    HashMap<Integer, ArrayList<Integer>> tempMap = new HashMap<Integer, ArrayList<Integer>>();

    public void processqueryterm(ArrayList<String> queries) throws FileNotFoundException
    {
        N = GlobalVariables.docMap.getSize();
        //Process each query
        for(int j = 0; j < queries.size(); j++)
        {
            queryTerm = queries.get(j);
            if(GlobalVariables.stopList.checkStopList(queryTerm) == false)
            {
                if(GlobalVariables.lexicon.checkForContentTerm(queryTerm))
                {
                    queryTerm = GlobalVariables.parser.parseQuery(queryTerm);
                    filePointer = GlobalVariables.lexicon.getPointer(queryTerm);
                    documentFrequency = (int) GlobalVariables.lexicon.getDocumentFrequency(queryTerm);
                    System.out.println("Processing Query " +queryTerm);
                    //Create file
                    File file = new File(invFile);
                    RandomAccessFile invListFile = new RandomAccessFile( file, "r" );
                    //Access file seek in inverted list at location specified in lexicon
                    try
                    {
                        //Fetch inverted list
                        invListFile.seek(filePointer);
                        //Get inter document frequency and document number
                        for(int i = 0; i < documentFrequency;i++)
                        {
                            // Display results and get document ID from map
                            documentID = invListFile.readInt();
                            interDocumentFrequency = invListFile.readInt();
                            documentLength = GlobalVariables.docMap.getDocumentLength(documentID);
                            similarity = GlobalVariables.similarityFunction.calculateBM25(N, documentFrequency, documentLength,interDocumentFrequency);
                            GlobalVariables.accumulatorsCollection.addToAccumulator(documentID, similarity);
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    //Set file name of inverted list
    public void setInvListFileName(String file)
    {
        invFile = file;
    }
}
