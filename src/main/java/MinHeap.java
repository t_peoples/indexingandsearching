import java.util.Arrays;
public class MinHeap <T extends Comparable<T>>
{
    private int capacity;
    private T[] heap;
    private int size;

    //Constructor
    @SuppressWarnings("unchecked")
    public MinHeap(int capacity)
    {
        this.capacity = capacity;
        heap = (T[]) new Comparable[capacity];
    }
    //Check if is empty
    public boolean isEmpty()
    {
        return size == 0;
    }
    //Insert into minheap
    public void insert(T t)
    {
        resize();
        heap[size++] = t;
        newhead(size - 1);
    }
    //Resize min heap
    private void resize()
    {
        if (size == capacity)
        {
            heap = Arrays.copyOf(heap, capacity * 2);
        }
        else if
        (size == capacity / 4) {
            heap = Arrays.copyOf(heap, capacity / 2);
        }
        capacity = heap.length;
    }
    //Replace head of minheap
    private void newhead(int i)
    {
        T temp = heap[i];
        while (i > 0 && temp.compareTo(heap[getParentIndex(i)]) < 0)
        {
            swap(i, getParentIndex(i));
            i = getParentIndex(i);
        }
    }
    //Get index of parent
    private int getParentIndex(int n)
    {
        return (n - 1) / 2;
    }
    //Swap nodes
    private void swap(int i, int j)
    {
        T temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;

    }
    //Get and Delete smallest node
    public T getMinimum()
    {
        if (isEmpty())
        {
            return null;
        }
        T min = gethead();
        heap[0] = heap[size - 1];
        heap[size - 1] = null;
        size--;
        movedown(0);
        resize();
        return min;
    }
    //Get head of minheap
    public T gethead()
    {
        if (isEmpty())
        {
            return null;
        }
        return heap[0];
    }
    //Move node down heap
    private void movedown(int i)
    {
        //Iterate through map
        while (hasLeftChild(i))
        {
            int leftChildIndex = getLeftIndex(i);
            int smallerChildIndex = leftChildIndex;
            //If has right child, compare
            if (hasRightChild(i))
            {
                if (heap[getRightIndex(i)].compareTo(heap[leftChildIndex]) < 0)
                {
                    smallerChildIndex = getRightIndex(i);
                }
            }
            //Compare with smaller index
            if (heap[i].compareTo(heap[smallerChildIndex]) > 0)
            {
                swap(i, smallerChildIndex);
            }
            else
            {
                break;
            }
            i = smallerChildIndex;
        }
    }
    private boolean hasLeftChild(int index)
    {
        return (2 * index + 1) <= (size - 1);
    }
    private boolean hasRightChild(int index)
    {
        return (2 * index + 2) <= (size - 1);
    }
    private int getLeftIndex(int n)
    {
        return 2 * n + 1;
    }
    private int getRightIndex(int n)
    {
        return 2 * n + 2;
    }
    public void printHeap()
    {
        for (int i = 0; i < heap.length; i++)
        {
            System.out.print(heap[i] + " ");
        }
        System.out.println();
    }
    //Delete specified node
    public T delete(T t)
    {
        //If is empty return null
        if (isEmpty())
        {
            return null;
        }
        //Iterate through heap
        for (int i = 0; i < heap.length; i++)
        {
            //Find and remove node
            if (heap[i].equals(t))
            {
                heap[i] = heap[size - 1];
                heap[size - 1] = null;
                size--;
                movedown(i);
                resize();
                return t;
            }
        }
        return null;
    }








}
