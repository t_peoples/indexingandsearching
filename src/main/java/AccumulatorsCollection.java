import java.util.ArrayList;
import java.io.IOException;


public class AccumulatorsCollection {

    ArrayList<Accumulator> accumulators = new ArrayList<Accumulator>();
    @SuppressWarnings("rawtypes")
    MinHeap minHeap;
    double scores[];
    int id;

    //Add score and document ID to accumulator
    public void addToAccumulator(int d, double score)
    {
        boolean added = checkAccumulator(d);

        if(added == false)
        {
            Accumulator accumulator = new Accumulator(d, score);
            accumulators.add(accumulator);
        }//If already in accumulator, update
        else
        {
            for(int i = 0; i < accumulators.size(); i++)
            {
                if(accumulators.get(i).getDocID() == d)
                {
                    accumulators.get(i).updateScore(score);
                }
            }
        }
    }
    //Check if is already in accumulator
    public boolean checkAccumulator(int d)
    {
        boolean value = false;
        for(int i = 0; i < accumulators.size(); i++)
        {
            if(accumulators.get(i).getDocID() == d)
            {
                value = true;
                return value;
            }
        }
        return value;
    }
    //Divide score by weights
    public void divideByWeights()
    {
        for(int i = 0; i < accumulators.size(); i++)
        {
            int id = accumulators.get(i).getDocID();
            double score = accumulators.get(i).getScore();
            double weight = GlobalVariables.docMap.getWeight(id);
            score = score / weight;
            accumulators.get(i).setScore(score);
        }
    }
    //TODO: Possible refactor, create seperate print function class
    //Prints query results with document summaries
    public void printResults() throws IOException
    {
        int n = 0;
        for(int i = 0; i < scores.length; i++)
        {
            id = findDocID(scores[i]);
            if(!(scores[i] == Double.POSITIVE_INFINITY))
            {
                System.out.print(GlobalVariables.queryLabel + " ");
                System.out.print(GlobalVariables.docMap.getDocumentID(id) + " ");
                System.out.print(n + " ");
                System.out.println(scores[i]);
                GlobalVariables.summarizer.printSummary(id);
                n++;
            }
            if(n == GlobalVariables.numberOfResults)
            {	break;
            }
        }
    }
    //TODO: Refactor move to minheap class
    //Get scores from minheap
    public void getMinHeap()
    {
        scores = new double [accumulators.size()];
        for(int i = scores.length - 1; i >= 0; i--)
        {
            double score = (Double) minHeap.getMinimum();
            scores[i] = score;
        }
    }
    //TODO: Refactor move to minheap class
    //Add to minheap
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void addToMinHeap() throws IOException
    {
        minHeap = new MinHeap(accumulators.size());
        for(int i = 0; i < accumulators.size();i++)
        {
            minHeap.insert(accumulators.get(i).getScore());
        }
    }
    //Find if for document using accumulator score
    public int findDocID(double score)
    {
        for(int i = 0; i < accumulators.size(); i++)
        {
            if(score == accumulators.get(i).getScore())
            {
                return accumulators.get(i).getDocID();
            }
        }
        return 0;
    }
    





}
