import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Map.Entry;

public class DocMap {

    //TreeMap<Integer, String> map = new TreeMap<Integer, String>();
    TreeMap<Integer,ArrayList<Object>> map2 = new TreeMap<Integer,ArrayList<Object>>();
    long fileLength;
    int documentOrdinalNumber;
    String documentIdentifier = null;
    int currentDocument;
    File file;
    //Each item in map contains a ordinal number and a document identifier
    //Reads map from file
    public void DocMap(String fileName){
        file =new File(fileName);
    }
    public void setFile(String fileName){
        file =new File(fileName);
    }
    public int getSize()
    {
        return map2.size();
    }
    //Get weight for document
    public double getWeight(int id)
    {
        ArrayList<Object> obj = map2.get(id);
        double weight = (double) obj.get(1);
        return weight;
    }
    //Get file pointer for start of document
    public long getStartPointer(int id)
    {
        long pointer = (long) map2.get(id).get(3);
        return pointer;
    }
    //Get file pointer for start of document
    public long getFinishPointer(int id)
    {
        long pointer = (long) map2.get(id).get(4);
        return pointer;
    }
    public void readMap() throws IOException
    {
        //if file doesnt exists, then create it
        if(!file.exists()){
            file.createNewFile();
        }
        RandomAccessFile mapFile = new RandomAccessFile( file, "r" );
        fileLength = mapFile.length();
        System.out.println("Reading document map");
        mapFile.seek(0);
        try
        {
            while (true)
            {
                int counter = mapFile.readInt();
                String msg = mapFile.readUTF();
                double weight = mapFile.readDouble();
                int dl = mapFile.readInt();
                long filePointerStart = mapFile.readLong();
                long filePointerFinish = mapFile.readLong();


                ArrayList<Object> mapVariables = new ArrayList<Object>();
                mapVariables.add(msg);
                mapVariables.add(weight);
                mapVariables.add(dl);
                mapVariables.add(filePointerStart);
                mapVariables.add(filePointerFinish);

                map2.put(counter,mapVariables);
            }
        }
        catch (EOFException ex)
        {
            System.out.println("End of file reached!");
        }
        finally
        {
            // Close the file to release file resources.
            mapFile.close();
        }
    }
    //Print out document map
    public void printMap()
    {
        for(Entry<Integer, ArrayList<Object>> entry : map2.entrySet())
        {
            System.out.print("Map no: " + entry.getKey()+",docName & Document Weight ");
            for(Object value : entry.getValue())
            {
                System.out.print(value + " ");
            }
            System.out.println(" ");
        }
    }
    //Check if exists already in map
    public boolean checkDocument(String documentID)
    {
        boolean hasDocumentBeenRead = map2.containsValue(documentID);
        return hasDocumentBeenRead;
    }
    //Get document ID
    public String getDocumentID(int key)
    {
        ArrayList<Object> obj = map2.get(key);
        String ID = (String) obj.get(0);
        return ID;
    }
    public int getDocumentLength(int docID)
    {
        ArrayList<Object> obj = map2.get(docID);
        int dl = (int) obj.get(2);
        return dl;
    }
    //Return average size of documents in the collection
    public double getAverageDocumentSize()
    {
        double average = 0;
        for(Entry<Integer, ArrayList<Object>> entry : map2.entrySet())
        {
            ArrayList<Object> doc = entry.getValue();
            average += (Integer) doc.get(2);
        }
        average = average / map2.size();
        return average;
    }
}
