/*
 * Tabitha Peoples
 * Class for storing program's global variables
 */
public class GlobalVariables
{

    public static DocMap docMap = new DocMap();
    public static Lexicon lexicon = new Lexicon();
    public static StopList stopList = new StopList();
    public static SimilarityFunction similarityFunction = new SimilarityFunction();
    public static ProcessQuery queryProcessor = new ProcessQuery();
    public static AccumulatorsCollection accumulatorsCollection = new AccumulatorsCollection();
    public static Summarizer summarizer = new Summarizer();
    public static Parser parser = new Parser();
    public static String queryLabel;
    public static Integer numberOfResults;
    public static int sumType;
    public static RunTimeCalculator runTimeCalculator = new RunTimeCalculator();



}