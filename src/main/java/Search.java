import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class Search {
        /** Name of class, used in error messages. */
        protected static final String progName = "Input";
        /**
         * Print help/usage message.
         */
        public static void main(String[] args) throws IOException
        {
            final long startTime = System.nanoTime();
            long duration;
            String mapFileName = null;
            String lexiconFileName = null;
            String invListFileName = null;
            String stopFileName;
            ArrayList<String> queries = new ArrayList<String>();
            // parse command line options
            OptionParser parser = new OptionParser("q:n:l:i:m:s:st:");
            OptionSet options = parser.parse(args);
            //TODO refactor to remove 'if' statements
            // -s <stoplist> specifies the stoplist file.
            if (options.has("s"))
            {
                if(options.hasArgument("s"))
                {
                    stopFileName = (String) options.valueOf("s");
                    GlobalVariables.stopList.setFileName(stopFileName);
                    GlobalVariables.stopList.readStopList();
                }
            }
            // -q <query-label> specifies the query label.
            if (options.has("q"))
            {
                if (options.hasArgument("q"))
                {
                    GlobalVariables.queryLabel = (String) options.valueOf("q");
                }
                else
                {
                    GlobalVariables.queryLabel = "1";
                }
            }
            // -n specifies the numbes of results.
            if (options.has("n"))
            {
                if (options.hasArgument("n"))
                {
                    String n = (String) options.valueOf("n");
                    GlobalVariables.numberOfResults = Integer.parseInt(n);
                }
                else
                {
                    GlobalVariables.numberOfResults = 1;
                }
            }
            // -m specifies the map.
            if (options.has("m"))
            {
                if (options.hasArgument("m"))
                {
                    mapFileName = "map";//(String) options.valueOf("m");
                }
            }
            // -s <query-label> specifies the lexicon.
            if (options.has("l"))
            {
                if (options.hasArgument("l"))
                {
                    lexiconFileName = (String) options.valueOf("l");
                }
            }
            // -i <query-label> specifies the invlist.
            if (options.has("i"))
            {
                if (options.hasArgument("i"))
                {
                    invListFileName = (String) options.valueOf("i");
                    GlobalVariables.queryProcessor.setInvListFileName(invListFileName);
                }
            }
            // -t <query-label> specifies the type of summary to be displayed.
            if (options.has("t"))
            {
                if (options.hasArgument("t"))
                {
                    //Set option 1 or 2 for printing summary
                    String t = (String) options.valueOf("t");
                    int sumType = Integer.parseInt(t);
                    GlobalVariables.sumType = sumType;
                }
                else
                {	//Print no summary
                    GlobalVariables.sumType = 3;
                }
            }
            // non option arguments
            List<?> tempArgs = options.nonOptionArguments();
            for (Object object : tempArgs) {
                //arguments.add((String) object);
                String qt = (String) object;
                queries.add(qt);
            }
            //TODO:refactor
            //Load map and lexicon into memory
            GlobalVariables.summarizer.setFileName("latimes");
            GlobalVariables.docMap.setFile(mapFileName);
            GlobalVariables.docMap.readMap();
            GlobalVariables.lexicon.setFile(lexiconFileName);
            GlobalVariables.lexicon.readLexicon();
            GlobalVariables.queryProcessor.processqueryterm(queries);
            GlobalVariables.accumulatorsCollection.divideByWeights();
            GlobalVariables.accumulatorsCollection.addToMinHeap();
            GlobalVariables.accumulatorsCollection.getMinHeap();
            GlobalVariables.accumulatorsCollection.printResults();
            //Calculate runtime
            GlobalVariables.runTimeCalculator.calculateRunTime(startTime);
        }

    }
