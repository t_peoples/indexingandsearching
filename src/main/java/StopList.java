import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;

public class StopList {

    String fileName;
    HashSet<String> stopList = new HashSet<String>();
    long fileLength;
    boolean stopListFlag;

    //Set file name of stoplist to be used
    public void setFileName(String file)
    {
        fileName = file;
    }
    //Return boolean is a stopped list to be used during this run of program
    public boolean getStopListFlag()
    {
        return stopListFlag;
    }
    //Check if word is in stoplist
    public boolean checkStopList(String word)
    {
        return stopList.contains(word);
    }
    public void readStopList() throws IOException
    {
        System.out.println("Reading stoplist");
        stopListFlag = true;
        //Create file
        File file =new File(fileName);
        //if file doesn't exists, then create it
        if(!file.exists()){
            file.createNewFile();
        }
        //Create random access file
        RandomAccessFile stopListFile = new RandomAccessFile( file, "r" );
        fileLength = stopListFile.length();
        stopListFile.seek(0);
        //Read stoplist file and add to hashset
        try
        {
            while (stopListFile.getFilePointer() < fileLength)
            {
                String stopListWord = stopListFile.readLine();
                stopList.add(stopListWord);
            }
        }
        catch (EOFException ex)
        {
            System.out.println("End of file reached!");
        }
        catch (IOException ex)
        {
            System.out.println("Input error");
        }
        finally
        {
            // Close the file to release file resources.
            stopListFile.close();
        }
    }
}
