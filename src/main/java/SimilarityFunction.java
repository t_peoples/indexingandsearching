public class SimilarityFunction {

    //Q = query terms
    //Dd = document in the collection
    double k;
    double k1 = 1.2;
    double b = 0.75;
    double AL;//Average Document Length
    int ld;//doc length
    double N;// = 131896 Documents in the collection
    double BM25;
    double ft; //Number of documents with the term
    int fdt; //Inter document frequency
    int docNumber;

    public void calculateK() {
        k = k1 * ((1 - b) + ((b * ld) / AL));
    }

    public double calculateBM25(double N, int ft, int ld, int fdt) {
        this.N = N;
        this.ft = ft;
        this.fdt = fdt;
        this.ld = ld;
        AL = GlobalVariables.docMap.getAverageDocumentSize();
        calculateK();
        BM25 = Math.log((N - ft + 0.5) / (ft + 0.5)) * (((k1 + 1) * fdt) / (k + fdt));
        return BM25;
    }

}
