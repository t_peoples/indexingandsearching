public class Parser {

    Boolean stopFlag = false;


    //Get whether stop list is to be used
    public void setStopListFlag()
    {
        stopFlag = GlobalVariables.stopList.getStopListFlag();
    }
    //Parse user query
    public String parseQuery(String query)
    {
        //Replace 's
        if(query.endsWith("'s"))
        {
            query = query.replace("'s","");
        }
        //Replace sses
        if(query.endsWith("sses"))
        {
            query = query.replace("sses","ss");
        }
        //Replace ies
        if(query.endsWith("ies"))
        {
            query = query.replace("ies","i");
        }
        query = query.toLowerCase().replaceAll("[^a-zA-Z]","");
        return query;
    }
    //TODO Refactor remove all these IF statements
    //Parse text for document summaries
    public String parseText(String query)
    {
        if(query.equals("</P>"))
        {
            query = query.replace("</P>"," ");
        }
        if(query.equals("</TEXT>"))
        {
            query = query.replace("</TEXT>"," ");
        }
        if(query.equals("<P>"))
        {
            query = query.replace("<P>"," ");
        }
        //Replace 's
        if(query.endsWith("'s"))
        {
            query = query.replace("'s","");
        }
        //Replace sses
        if(query.endsWith("sses"))
        {
            query = query.replace("sses","ss");
        }
        //Replace ies
        if(query.endsWith("ies"))
        {
            query = query.replace("ies","i");
        }
        query = query.toLowerCase().replaceAll("[^a-zA-Z]","");
        return query;
    }

}


}
