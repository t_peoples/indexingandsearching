import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class Summarizer {

    String fileName;
    long startPointer;
    long finishPointer;
    int pCount = 0;
    double QBs;
    ArrayList<String> sentences = new ArrayList<String>();
    String[] parts;
    String sentence = null;
    ArrayList<Sentence> sentencesWithValues = new ArrayList<Sentence>();
    RandomAccessFile rf;

    //Set file name of document collection
    public void setFileName(String name)
    {
        fileName = name;
    }
    public void printSummary(int docID) throws IOException
    {
        if(GlobalVariables.sumType == 1)
        {
            querySummarisation(docID);
        }
        if(GlobalVariables.sumType == 2)
        {
            readFirstChunk(docID);
        }
    }
    //Calculate query values for each sentence and add to array list
    private void calculateQueryBiasedSummarisation(String sentence)
    {
        int qts = getNumberOfUniqueQueryTerms(sentence);
        parts = sentence.split(" ");

        //Parse text to avoid affectin scores
        for(int i = 0; i < parts.length; i++ )
        {
            parts[i] = GlobalVariables.parser.parseText(parts[i]);
        }
        int qt = parts.length;
        QBs = Math.pow(qts, 2) / qt;
        Sentence s = new Sentence(sentence,QBs);
        sentencesWithValues.add(s);
    }
    //Each sentence has a number of unique terms in it, so removes duplicates
    private int getNumberOfUniqueQueryTerms(String sentence)
    {
        parts = sentence.split(" ");
        for(int i = 0; i < parts.length; i++)
        {
            parts[i] = GlobalVariables.parser.parseText(parts[i]);
        }
        ArrayList<String> words = new ArrayList<String>();
        for(int i = 0; i < parts.length; i++)
        {
            if(!words.contains(parts[i]))
            {
                words.add(parts[i]);
            }
        }
        return words.size();
    }
    //Read document from collection and produce a summary based on the query
    //Summarization algorithm
    public void querySummarisation(int id) throws IOException
    {
        try
        {
            //Create file
            File file =new File(fileName);
            startPointer = GlobalVariables.docMap.getStartPointer(id);
            finishPointer = GlobalVariables.docMap.getFinishPointer(id);
            rf = new RandomAccessFile( file, "r" );
            String line;
            rf.seek(startPointer);
            while (rf.getFilePointer() != finishPointer)
            {
                line = rf.readLine();

                if(line.contains("."))
                {
                    String[] parts = line.split("\\.");
                    sentence += parts[0];
                    sentences.add(sentence);
                    for(int i = 1; i < parts.length - 1; i++)
                    {
                        sentence = parts[i];
                        sentences.add(sentence);
                    }
                    sentence = parts[parts.length - 1];
                }
                else
                {
                    sentence += line;
                }
            }
        }
        catch (EOFException ex)
        {
            System.out.println("End of file reached!");
        }
        catch (NullPointerException ex)
        {
            System.out.println("Error no source file entered");
        }
        finally
        {
            // Close the file to release file resources.
            rf.close();
        }
        for(int i = 0; i < sentences.size();i++)
        {
            calculateQueryBiasedSummarisation(sentences.get(i));
        }
        sentences.clear();
        //PriorityQueue example with Comparator
        Queue<Sentence> sentencePriorityQueue = new PriorityQueue<>(sentencesWithValues.size(), sentenceResultsComparator);
        addDataToQueue(sentencePriorityQueue);
        pollDataFromQueue(sentencePriorityQueue);
    }
    //Comparator anonymous class implementation
    private static Comparator<Sentence> sentenceResultsComparator = new Comparator<Sentence>()
    {
        @Override
        public int compare(Sentence s1, Sentence s2)
        {
            //  return (int) (c1.getScore() - c2.getScore());
            if (s1.getScore() > s2.getScore()) return -1;
            if (s1.getScore() < s2.getScore()) return 1;
            return 0;
        }
    };
    //Add data to priority queue
    private void addDataToQueue(Queue<Sentence> sentencePriorityQueue)
    {
        for(int i=0; i <sentencesWithValues.size(); i++)
        {
            sentencePriorityQueue.add(sentencesWithValues.get(i));
        }
        sentencesWithValues.clear();
    }
    //utility method to poll data from queue
    private static void pollDataFromQueue(Queue<Sentence> sentencePriorityQueue)
    {
        int topSentences = 0;
        for(int i = 0; i < sentencePriorityQueue.size();i++)
        {
            Sentence s = sentencePriorityQueue.remove();
            //	if(s == null) break;
            System.out.println(s.getline());
            topSentences++;

            if(topSentences == 5)
                break;
        }
        sentencePriorityQueue.clear();
    }
    //Read first section of document and display to user
    public void readFirstChunk(int id) throws IOException
    {
        //Create file
        File file =new File(fileName);
        startPointer = GlobalVariables.docMap.getStartPointer(id);
        finishPointer = GlobalVariables.docMap.getFinishPointer(id);
        RandomAccessFile rf = new RandomAccessFile( file, "r" );
        String line;
        rf.seek(startPointer);
        try
        {
            while (rf.getFilePointer() != finishPointer)
            {
                line = rf.readLine();
                System.out.println(line);
                if(line.contentEquals("</P>"))
                {
                    break;
                }
            }
        }
        catch (EOFException ex)
        {
            System.out.println("End of file reached!");
        }
        finally
        {
            // Close the file to release file resources.
            rf.close();
        }
    }






}
