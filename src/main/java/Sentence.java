public class Sentence {
    private String line;
    private Double score;

    //Constructor
    public Sentence(String line, double score)
    {
        this.line = line;
        this.score = score;
    }
    public String getline()
    {
        return line;
    }
    public double getScore()
    {
        return score;
    }
}

